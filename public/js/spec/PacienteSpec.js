describe("Paciente", function () {

    it("deve calcular o imc", function () {
        var lucas = new Paciente("Lucas", 25, 85, 1.70);

        var imc = lucas.imc();

        expect(imc).toEqual(85/(1.70 * 1.70));
    });


    it("deve calcular o imc 2", function () {
        var lucas = new Paciente("Lucas", 25, 70, 1.80);

        var imc = lucas.imc();

        expect(imc).toEqual(70/(1.80 * 1.80));
    });

    it("deve calcular os batimentos", function () {
        var lucas = new Paciente("Lucas", 25, 70, 1.80);

        expect(lucas.batimentos()).toEqual(1051200000);
    });

});