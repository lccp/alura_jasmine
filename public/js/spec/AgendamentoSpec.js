describe("Agendamento", function () {

    var lucas;
    var agendamento;

    beforeEach(function () {
        lucas = PacienteBuilder().constroi();
        agendamento = new Agendamento();
    });

    it("Deve agendar para 20 dias depois", function () {

        var consulta = new Consulta(lucas, [], false, false, new Date(2014, 7, 1));
        var novaConsulta = agendamento.para(consulta);


        expect(novaConsulta.getData().toString()).toEqual(new Date(2014, 7, 21).toString());

    });

    it("Deve pular fins de semana", function () {

        var consulta = new Consulta(lucas, [], false, false, new Date(2014, 5, 30));
        var novaConsulta = agendamento.para(consulta);


        expect(novaConsulta.getData().toString()).toEqual(new Date(2014, 6, 21).toString());

    });


});
