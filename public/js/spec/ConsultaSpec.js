describe("Consulta", function () {

    var lucas;

    beforeEach(function () {
        lucas = new PacienteBuilder().constroi();
    });



    it("não deve cobrar nada se for um retorno", function () {

        var consulta = new Consulta(lucas, [], true, true);


        expect(consulta.preco()).toEqual(0);
    });

    describe("Consultas com procedimentos", function () {
        it("deve cobrar 25 por cada procedimento comum", function () {

            var consulta = new Consulta(lucas, ["proc1", "proc2"], false, false);


            expect(consulta.preco()).toEqual(50);
        });


        it("deve cobrar preco especifico por cada procedimento", function () {

            var consulta = new Consulta(lucas, ["proc-comum", "raio-x","proc-comum", "gesso"], false, false);


            expect(consulta.preco()).toEqual(25+55+25+32);
        });
    });

    describe("Consultas particulares", function () {
        it("deve cobrar o dobro por ser particular", function () {

            var consulta = new Consulta(lucas, ["raio-x", "gesso"], true, false);

            expect(consulta.preco()).toEqual(174);
        });
    });



});
